package com.dashney.SpringSecurity.appuser;

import net.bytebuddy.dynamic.DynamicType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface StudentRepository extends JpaRepository<ApplicationUser,Long> {

    Optional<ApplicationUser> findByEmail(String email);
}
