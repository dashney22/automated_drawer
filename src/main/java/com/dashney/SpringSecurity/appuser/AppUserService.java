package com.dashney.SpringSecurity.appuser;

import com.dashney.SpringSecurity.registration.token.ConfirmationToken;
import com.dashney.SpringSecurity.registration.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService{
    private final static String USER_NOT_FOUND_MESSAGE = "user with %s not found";
    private final StudentRepository studentRepository;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmationTokenService confirmationTokenService;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return studentRepository.findByEmail(email)
                .orElseThrow(()-> new UsernameNotFoundException(
                        String.format(USER_NOT_FOUND_MESSAGE,email)
                ));
    }

    public String signUpUser(ApplicationUser applicationUser){
        boolean userExist = studentRepository.findByEmail(applicationUser.getEmail())
                .isPresent();

        if(userExist){
            throw new IllegalStateException("Email already taken");
        }

        String encodedPassword = passwordEncoder.encode(applicationUser.getPassword());
        applicationUser.setPassword(encodedPassword);
        studentRepository.save(applicationUser);
        //AFTER TOKEN CREATION
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                applicationUser
        );
        confirmationTokenService.saveConfirmationToken(confirmationToken);

        // SEND EMAIL
        return token;
    }
}
