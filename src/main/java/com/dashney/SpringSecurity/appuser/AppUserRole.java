package com.dashney.SpringSecurity.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
